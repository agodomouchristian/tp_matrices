CC = gcc


matrice: matrice.o main.o
	gcc matrice.o main.o -o matrice -lm -fsanitize=address -g
matrice.o: matrice.c matrice.h
	gcc -c matrice.c
main.o: main.c
	gcc -c main.c
clean:
	rm -f *.o matrice 
rebuild: clean matrice 