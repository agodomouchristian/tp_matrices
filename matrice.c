#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include "matrice.h"

error_code matrix_print(const matrix mat){

    printf("Matrice:\n\n");
    for(int i = 0; i < mat.m; i++){
        for(int j = 0; j < mat.n; j++){

            printf("%d  ", mat.data[i][j]);
        }
        printf("\n");
    }
    return ok;
}


error_code matrix_alloc(matrix *mat, int32_t m, int32_t n){
    //matrice mat;
    mat->m = m;
    mat->n = n;
    mat->data = malloc(m * sizeof(int32_t*));
    for(int i = 0; i < mat->m; i++){
        mat->data[i] = malloc(n * sizeof(int32_t));
    }
    return ok;
}

error_code matrix_init(matrix *mat, int32_t m, int32_t n, int32_t val){
    //matrix_alloc(mat,m,n);
    for(int i=0 ; i < m ; i++){
        for(int j = 0 ; j < n ; j++){
            mat->data[i][j]=val ;
        }
    }
    return ok;
}

error_code matrix_destroy(matrix *mat){
    
    for(int i=0 ; i < mat->m ; i++){
        free(mat->data[i]);
        
    }
    free(mat->data);
    mat->data = NULL;
    mat->m = -1;
    mat->n = -1;
    return 0;
}

error_code matrix_init_from_array(matrix *mat, int32_t m, int32_t n,int32_t data[], int32_t s){
    int index = 0;
    for(int i=0 ; i < m ; i++){
        for(int j = 0 ; j < n ; j++){
            mat->data[i][j]= data[index] ;
            index++;
        }
    }
    return ok;
}

error_code matrix_clone(matrix *cloned, const matrix mat){
    //matrix_alloc(cloned,mat.m,mat.n);
    for(int i=0 ; i < mat.m ; i++){
        for(int j = 0 ; j < mat.n ; j++){
            cloned->data[i][j]= mat.data[i][j] ;
            
        }
    }
    return ok;
}

error_code matrix_transpose(matrix *transposed,const matrix mat){

    matrix_alloc(transposed,mat.m,mat.n);
    for(int i=0 ; i < mat.m ; i++){
        for(int j = 0 ; j < mat.n ; j++){
            transposed->data[j][i]= mat.data[i][j];
            
        }
    }
    return ok;
}

error_code matrix_extract_submatrix(matrix *sub,const matrix mat,int32_t m0, int32_t m1, int32_t n0, int32_t n1){
    if (m0 < 0 || m1 > mat.m || n0 < 0 || n1 > mat.n || m0 > m1 || n0 > n1)
        return err;
    else{
        matrix_alloc(sub , ((m1+1)-m0), ((n1+1)-n0));
        int x = 0;
        int y = 0;
        for(int i = m0; i <= m1; i++){
            for(int j = n0; j <= n1; j++){
                sub->data[x][y] = mat.data[i][j];
                y++;
            }
            x++;
            y = 0;
        }
        return ok;
    }
        
}

bool matrix_is_equal(matrix mat1, matrix mat2){
    if (mat1.m == mat2.m && mat1.n == mat2.n)
        return 1;
    else{
        return 0;
    } 
    return ok;
}

