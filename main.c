#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>
#include "matrice.h"


void main() {
    /*
    matrix *mat = malloc(sizeof(matrix));
    matrix_alloc(mat,2,3);
    //matrix_init(mat, 2, 3, 1);
    int32_t data[] = {0,1,2,3,4,5};
    matrix_init_from_array(mat,  2,  3, data,  6)
    */
    /*
    matrix *mat = malloc(sizeof(matrix));
    matrix const mat_2 = *mat;
    matrix_alloc(mat,2,3);
    matrix_init(mat, 2, 3, 1);
    matrix_print(*mat);
    matrix_clone(mat,mat_2);
    */
    matrix *mat = malloc(sizeof(matrix));
    matrix_alloc(mat,2,3);
    matrix_init(mat, 2, 3, 1);
    matrix const mat_2 = *mat;
    //matrix_transpose(mat,mat_2);
    matrix_print(*mat);

    matrix_destroy(mat);

    free(mat);





}